# Turkish translation for Loupe.
# Copyright (C) 2023 Loupe's COPYRIGHT HOLDER
# This file is distributed under the same license as the Loupe package.
#
# Sabri Ünal <libreajans@gmail.com>, 2023.
# Emin Tufan Çetin <etcetin@gmail.com>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: Loupe main\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/loupe/-/issues\n"
"POT-Creation-Date: 2024-02-13 22:38+0000\n"
"PO-Revision-Date: 2024-02-14 17:42+0300\n"
"Last-Translator: Sabri Ünal <libreajans@gmail.com>\n"
"Language-Team: Turkish <takim@gnome.org.tr>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 3.4.2\n"

#: data/org.gnome.Loupe.desktop.in.in:3
#: data/org.gnome.Loupe.metainfo.xml.in.in:6 src/about.rs:29 src/window.rs:880
msgid "Image Viewer"
msgstr "Resim Görüntüleyici"

#: data/org.gnome.Loupe.desktop.in.in:11
msgid "Picture;Graphics;Loupe;"
msgstr "Resim;Fotoğraf;Grafikler;Görseller;Loupe;"

#: data/org.gnome.Loupe.gschema.xml.in:6
msgid "Show the image properties sidebar"
msgstr "Resim özellikleri kenar çubuğunu göster"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.Loupe.metainfo.xml.in.in:8 src/about.rs:32
msgid "The GNOME Project"
msgstr "GNOME Projesi"

#: data/org.gnome.Loupe.metainfo.xml.in.in:12
msgid "View images"
msgstr "Resimleri göster"

#: data/org.gnome.Loupe.metainfo.xml.in.in:14
msgid "Browse through your images and inspect their metadata with:"
msgstr "Resimlerinize göz atın ve üst verilerini şunlarla inceleyin:"

#: data/org.gnome.Loupe.metainfo.xml.in.in:16
msgid "Fast GPU accelerated image rendering"
msgstr "Hızlı, GPU hızlandırmalı resim sahneleme"

#: data/org.gnome.Loupe.metainfo.xml.in.in:17
msgid "Tiled rendering for vector graphics"
msgstr "Vektör grafikler için bölünmüş sahneleme"

#: data/org.gnome.Loupe.metainfo.xml.in.in:18
msgid "Extendable and sandboxed image decoding"
msgstr "Genişletilebilir ve yalıtılmış resim çözme"

#: data/org.gnome.Loupe.metainfo.xml.in.in:19
msgid "Accessible presentation of the most important metadata."
msgstr "Çoğu önemli üst verinin erişilebilir gösterimi."

#: data/resources/gtk/help-overlay.ui:11
msgctxt "shortcut window"
msgid "General Shortcuts"
msgstr "Genel Kısayollar"

#: data/resources/gtk/help-overlay.ui:14
msgctxt "shortcut window"
msgid "Show Main Menu"
msgstr "Ana Menüyü Göster"

#: data/resources/gtk/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Copy Image or Metadata to Clipboard"
msgstr "Resmi Ya Da Üst Veriyi Panoya Kopyala"

#: data/resources/gtk/help-overlay.ui:26
msgctxt "shortcut window"
msgid "Move Image to Trash"
msgstr "Resmi Çöpe Taşı"

#: data/resources/gtk/help-overlay.ui:32
msgctxt "shortcut window"
msgid "Permanently Delete Image"
msgstr "Resmi Kalıcı Olarak Sil"

#: data/resources/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Toggle Image Properties"
msgstr "Resim Özelliklerini Aç/Kapat"

#: data/resources/gtk/help-overlay.ui:44
msgctxt "shortcut window"
msgid "Set as Background"
msgstr "Arka Plan Olarak Ayarla"

#: data/resources/gtk/help-overlay.ui:50
msgctxt "shortcut window"
msgid "Toggle Fullscreen"
msgstr "Tam Ekranı Aç/Kapat"

#: data/resources/gtk/help-overlay.ui:56
msgctxt "shortcut window"
msgid "Leave Fullscreen"
msgstr "Tam Ekrandan Çık"

#: data/resources/gtk/help-overlay.ui:62
msgctxt "shortcut window"
msgid "New Window"
msgstr "Yeni Pencere"

#: data/resources/gtk/help-overlay.ui:68
msgctxt "shortcut window"
msgid "Open Help"
msgstr "Yardımı Aç"

#: data/resources/gtk/help-overlay.ui:74
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "Klavye Kısayollarını Göster"

#: data/resources/gtk/help-overlay.ui:80
msgctxt "shortcut window"
msgid "Close Window"
msgstr "Pencereyi Kapat"

#: data/resources/gtk/help-overlay.ui:86
msgctxt "shortcut window"
msgid "Quit"
msgstr "Çık"

#: data/resources/gtk/help-overlay.ui:94
msgctxt "shortcut window"
msgid "Navigate Images"
msgstr "Resimlerde Gezin"

#: data/resources/gtk/help-overlay.ui:98 data/resources/gtk/help-overlay.ui:112
#: data/resources/gtk/help-overlay.ui:224
#: data/resources/gtk/help-overlay.ui:238
msgctxt "shortcut window"
msgid "Next Image"
msgstr "Sonraki Resim"

#: data/resources/gtk/help-overlay.ui:105
#: data/resources/gtk/help-overlay.ui:119
#: data/resources/gtk/help-overlay.ui:231
#: data/resources/gtk/help-overlay.ui:245
msgctxt "shortcut window"
msgid "Previous Image"
msgstr "Önceki Resim"

#: data/resources/gtk/help-overlay.ui:125
msgctxt "shortcut window"
msgid "First Image"
msgstr "İlk Resim"

#: data/resources/gtk/help-overlay.ui:131
msgctxt "shortcut window"
msgid "Last Image"
msgstr "Son Resim"

#: data/resources/gtk/help-overlay.ui:139
msgctxt "shortcut window"
msgid "Rotate Image"
msgstr "Resmi Döndür"

#: data/resources/gtk/help-overlay.ui:142
#: data/resources/gtk/help-overlay.ui:263
msgctxt "shortcut window"
msgid "Rotate Clockwise"
msgstr "Saat Yönünde Döndür"

#: data/resources/gtk/help-overlay.ui:148
#: data/resources/gtk/help-overlay.ui:269
msgctxt "shortcut window"
msgid "Rotate Counterclockwise"
msgstr "Saat Yönünün Tersine Döndür"

#: data/resources/gtk/help-overlay.ui:156
msgctxt "shortcut window"
msgid "Zoom Image"
msgstr "Resim Yakınlaştırma"

#: data/resources/gtk/help-overlay.ui:159
#: data/resources/gtk/help-overlay.ui:251
msgctxt "shortcut window"
msgid "Zoom In"
msgstr "Yakınlaştır"

#: data/resources/gtk/help-overlay.ui:165
#: data/resources/gtk/help-overlay.ui:257
msgctxt "shortcut window"
msgid "Zoom Out"
msgstr "Uzaklaştır"

#: data/resources/gtk/help-overlay.ui:171
msgctxt "shortcut window"
msgid "Best Fit"
msgstr "En Uygun"

#: data/resources/gtk/help-overlay.ui:177
msgctxt "shortcut window"
msgid "Original Scale"
msgstr "Özgün Ölçekleme"

#: data/resources/gtk/help-overlay.ui:183
msgctxt "shortcut window"
msgid "200% Scale"
msgstr "%200 Ölçekleme"

#: data/resources/gtk/help-overlay.ui:191
msgctxt "shortcut window"
msgid "Pan View"
msgstr "Görünüm Kaydırma"

#: data/resources/gtk/help-overlay.ui:194
msgctxt "shortcut window"
msgid "Pan Left"
msgstr "Sola Kaydır"

#: data/resources/gtk/help-overlay.ui:200
msgctxt "shortcut window"
msgid "Pan Right"
msgstr "Sağa Kaydır"

#: data/resources/gtk/help-overlay.ui:206
msgctxt "shortcut window"
msgid "Pan Up"
msgstr "Yukarı Kaydır"

#: data/resources/gtk/help-overlay.ui:212
msgctxt "shortcut window"
msgid "Pan Down"
msgstr "Aşağı Kaydır"

#: data/resources/gtk/help-overlay.ui:220
msgctxt "shortcut window"
msgid "Gestures"
msgstr "Hareketler"

#. Translators: Replace "translator-credits" with your names, one name per line
#: src/about.rs:41
msgid "translator-credits"
msgstr ""
"Sabri Ünal <libreajans@gmail.com>\n"
"Emin Tufan Çetin <etcetin@gmail.com>"

#: src/about.rs:42
msgid "Copyright © 2020–2024 Christopher Davis et al."
msgstr "Telif Hakkı © 2020–2024 Christopher Davis ve ark."

#: src/file_model.rs:128
msgid "Could not list other files in directory."
msgstr "Dizindeki diğer dosyalar listelenemedi."

#. Translators: short for "north" in GPS coordinate
#: src/metadata/gps.rs:165
msgid "N"
msgstr "K"

#. Translators: short for "south" in GPS coordinate
#: src/metadata/gps.rs:168
msgid "S"
msgstr "G"

#. Translators: short for "east" in GPS coordinate
#: src/metadata/gps.rs:190
msgid "E"
msgstr "D"

#. Translators: short for "west" in GPS coordinate
#: src/metadata/gps.rs:193
msgid "W"
msgstr "B"

#. Translators: Unit for exposure time in seconds
#: src/metadata/mod.rs:260
msgid "1\\u{2215}{}\\u{202F}s"
msgstr "1\\u{2215}{}\\u{202F}sn"

#. Translators: Unit for focal length in millimeters
#: src/metadata/mod.rs:285
msgid "{}\\u{202F}mm"
msgstr "{}\\u{202F}mm"

#. Translators: This is the date and time format we use in metadata output etc.
#. The format has to follow <https://docs.gtk.org/glib/method.DateTime.format.html>
#. The default is already translated. Don't change if you are not sure what to
#. use.
#: src/util/mod.rs:37
#, c-format
msgid "%x %X"
msgstr "%x %X"

#: src/util/mod.rs:180 src/window.rs:701
msgid "Failed to restore image from trash"
msgstr "Resim çöpten geri yüklenemedi"

#: src/util/mod.rs:201
msgid "Failed to open image"
msgstr "Resim açılamadı"

#: src/widgets/image_page.ui:6 src/window.ui:16
msgid "_Open With…"
msgstr "Birlikte _Aç…"

#: src/widgets/image_page.ui:14 src/window.ui:24
msgid "_Print…"
msgstr "_Yazdır…"

#: src/widgets/image_page.ui:18
msgid "_Copy"
msgstr "K_opyala"

#: src/widgets/image_page.ui:23 src/window.ui:29
msgid "Rotate"
msgstr "Döndür"

#: src/widgets/image_page.ui:34 src/window.ui:40
msgid "_Set as Background…"
msgstr "_Arka Plan Olarak Ayarla…"

#: src/widgets/image_page.ui:63
msgid "Could not Load Image"
msgstr "Resim Yüklenemedi"

#: src/widgets/image_page.ui:97 src/window.ui:106
msgid "Rotate Left"
msgstr "Sola Döndür"

#: src/widgets/image_page.ui:107 src/window.ui:116
msgid "Rotate Right"
msgstr "Sağa Döndür"

#. Translators: This is a toast notification, informing the user that
#. an image has been set as background.
#: src/widgets/image_view.rs:682
msgid "Set as background."
msgstr "Arka plan olarak ayarla."

#: src/widgets/image_view.rs:695
msgid "Could not set background."
msgstr "Arka plan ayarlanamadı."

#: src/widgets/image_view.ui:27
msgid "Previous Image"
msgstr "Önceki Resim"

#: src/widgets/image_view.ui:38
msgid "Next Image"
msgstr "Sonraki Resim"

#: src/widgets/image_view.ui:63
msgid "Zoom Out"
msgstr "Uzaklaştır"

#: src/widgets/image_view.ui:74
msgid "Zoom In"
msgstr "Yakınlaştır"

#: src/widgets/image_view.ui:85
msgid "Toggle Fullscreen"
msgstr "Tam Ekranı Aç/Kapat"

#: src/widgets/print.rs:116
msgid "Top"
msgstr "Üst"

#: src/widgets/print.rs:117
msgid "Center"
msgstr "Merkez"

#: src/widgets/print.rs:118
msgid "Bottom"
msgstr "Alt"

#: src/widgets/print.rs:119
msgid "Left"
msgstr "Left"

#: src/widgets/print.rs:120
msgid "Right"
msgstr "Sağ"

#. Translators: Shorthand for centimeter
#: src/widgets/print.rs:156
msgid "cm"
msgstr "cm"

#. Translators: Shorthand for inch
#: src/widgets/print.rs:158
msgid "in"
msgstr "in"

#. Translators: Shorthand for pixel
#: src/widgets/print.rs:160
msgid "px"
msgstr "px"

#. Translators: {} is a placeholder for the filename
#: src/widgets/print.rs:371
msgid "Print “{}”"
msgstr "“{}” yazdır"

#: src/widgets/print.rs:782
msgid "_Scale"
msgstr "_Ölçekle"

#: src/widgets/print.rs:785
msgid "_Width"
msgstr "_Genişlik"

#. Translators: Go back to previous page
#: src/widgets/print.ui:29
msgid "_Back"
msgstr "_Geri"

#: src/widgets/print.ui:37
msgid "_Print"
msgstr "_Yazdır"

#: src/widgets/print.ui:56
msgid "Layout"
msgstr "Düzen"

#: src/widgets/print.ui:59
msgid "_Orientation"
msgstr "_Yönelim"

#: src/widgets/print.ui:72
msgid "Portrait Page Orientation"
msgstr "Dikey Sayfa Yönelimi"

#: src/widgets/print.ui:80
msgid "Landscape Page Orientation"
msgstr "Yatay Sayfa Yönelimi"

#: src/widgets/print.ui:89
msgid "_Alignment"
msgstr "_Hizalama"

#: src/widgets/print.ui:108
msgid "Margins"
msgstr "Kenar Boşlukları"

#: src/widgets/print.ui:111
msgid "Margin Units"
msgstr "Kenar Boşluğu Birimleri"

#: src/widgets/print.ui:128
msgid "_Horizontal"
msgstr "_Yatay"

#: src/widgets/print.ui:134
msgid "_Vertical"
msgstr "_Dikey"

#: src/widgets/print.ui:142 src/widgets/properties_view.ui:53
msgid "Image Size"
msgstr "Resim Boyutu"

#: src/widgets/print.ui:145
msgid "Image Size Units"
msgstr "Resim Boyutu Birimleri"

#: src/widgets/print.ui:163
msgid "_Fill Space"
msgstr "_Dolgu Boşluğu"

#: src/widgets/print.ui:174
msgid "H_eight"
msgstr "_Yükseklik"

#: src/widgets/properties_view.rs:234
msgid "Unknown"
msgstr "Bilinmeyen"

#. Translators: Addition of image being transparent to format name
#: src/widgets/properties_view.rs:238
msgid ", transparent"
msgstr ", saydam"

#. Translators: Addition of image being grayscale to format name
#: src/widgets/properties_view.rs:243
msgid ", grayscale"
msgstr ", gri tonlama"

#. Translators: Addition of bit size to format name
#: src/widgets/properties_view.rs:248
msgid ", {}\\u{202F}bit"
msgstr ", {}\\u{202F}bit"

#: src/widgets/properties_view.rs:299
msgid "Image Coordinates Copied"
msgstr "Resim Koordinatları Kopyalandı"

#: src/widgets/properties_view.ui:19
msgid "Folder"
msgstr "Klasör"

#: src/widgets/properties_view.ui:26
msgid "Open Containing Folder"
msgstr "İçeren Klasörü Aç"

#: src/widgets/properties_view.ui:39
msgid "URI"
msgstr "URI"

#: src/widgets/properties_view.ui:62
msgid "Image Format"
msgstr "Resim Biçimi"

#: src/widgets/properties_view.ui:71
msgid "File Size"
msgstr "Dosya Boyutu"

#: src/widgets/properties_view.ui:84
msgid "File Created"
msgstr "Oluşturulma"

#: src/widgets/properties_view.ui:93
msgid "File Modified"
msgstr "Değiştirilme"

#: src/widgets/properties_view.ui:106
msgid "Location"
msgstr "Konum"

#: src/widgets/properties_view.ui:116
msgid "Copy Coordinates"
msgstr "Koordinatları Kopyala"

#: src/widgets/properties_view.ui:127
msgid "Open Location"
msgstr "Konumu Aç"

#: src/widgets/properties_view.ui:140
msgid "Originally Created"
msgstr "Özgün Oluşturulma"

#: src/widgets/properties_view.ui:149
msgid "Aperture"
msgstr "Açıklık"

#: src/widgets/properties_view.ui:158
msgid "Exposure"
msgstr "Pozlama"

#: src/widgets/properties_view.ui:167
msgid "ISO"
msgstr "ISO"

#: src/widgets/properties_view.ui:176
msgid "Focal Length"
msgstr "Odak Uzaklığı"

#: src/widgets/properties_view.ui:185
msgid "Maker, Model"
msgstr "Üretici, Model"

#: src/window.rs:568
msgid "Supported image formats"
msgstr "Desteklenen resim biçimleri"

#: src/window.rs:574
msgid "All files"
msgstr "Tüm dosyalar"

#: src/window.rs:581
msgid "Open Image"
msgstr "Resim Aç"

#: src/window.rs:667
msgid "Image copied to clipboard"
msgstr "Resim panoya kopyalandı"

#: src/window.rs:688
msgid "Image moved to trash"
msgstr "Resim çöpe taşındı"

#: src/window.rs:689
msgid "Undo"
msgstr "Geri Al"

#: src/window.rs:715
msgid "Failed to move image to trash"
msgstr "Resim çöpe taşınamadı"

#: src/window.rs:741
msgid "Permanently Delete Image?"
msgstr "Resim Kalıcı Olarak Silinsin Mi?"

#: src/window.rs:743
msgid "After deleting the image “{}” it will be permanently lost."
msgstr "“{}” resmi silindikten sonra kalıcı olarak yitirilecek."

#: src/window.rs:751
msgid "Cancel"
msgstr "İptal"

#: src/window.rs:752
msgid "Delete"
msgstr "Sil"

#: src/window.rs:763
msgid "Failed to delete image"
msgstr "Resim silinemedi"

#: src/window.ui:6
msgid "_New Window"
msgstr "Ye_ni Pencere"

#: src/window.ui:10
msgid "_Open…"
msgstr "_Aç…"

#: src/window.ui:46
msgid "_Help"
msgstr "_Yardım"

#: src/window.ui:50
msgid "_Keyboard Shortcuts"
msgstr "_Klavye Kısayolları"

#: src/window.ui:54
msgid "_About Image Viewer"
msgstr "Resim Görüntüleyici _Hakkında"

#: src/window.ui:85
msgid "Copy to Clipboard"
msgstr "Panoya Kopyala"

#: src/window.ui:92
msgid "Move to Trash"
msgstr "Çöpe Taşı"

#: src/window.ui:98
msgid "Main Menu"
msgstr "Ana Menü"

#: src/window.ui:130
msgid "Image Properties"
msgstr "Resim Özellikleri"

#: src/window.ui:174
msgid "View Images"
msgstr "Resimleri Göster"

#: src/window.ui:175
msgid "Drag and drop images here"
msgstr "Resimleri sürükleyip buraya bırakın"

#: src/window.ui:179
msgid "_Open Files…"
msgstr "Dosya _Aç…"

#~ msgid "The Loupe Team"
#~ msgstr "Loupe Takımı"

#~ msgid "Center image horizontally"
#~ msgstr "Resmi yatay ortala"

#~ msgid "Center image vertically"
#~ msgstr "Resmi dikey ortala"

#~ msgid "Image Scale"
#~ msgstr "Resim Ölçekle"

#~ msgid "Left Margin"
#~ msgstr "Sol Kenar Boşluğu"

#~ msgid "Length Unit"
#~ msgstr "Uzunluk Birimi"

#~ msgid "_About Loupe"
#~ msgstr "Loupe _Hakkında"

#~ msgid "Loupe"
#~ msgstr "Loupe"

#~ msgid "Failed to read image file information"
#~ msgstr "Görüntü dosyası bilgisi okunamadı"

#~ msgid "Failed to decode image"
#~ msgstr "Görüntü kodu çözülemedi"

#~ msgid "Unknown image format"
#~ msgstr "Bilinmeyen görüntü biçimi"

#~ msgid "Animated GIF"
#~ msgstr "Canlandırmalı GIF"

#~ msgid "Animated WebP"
#~ msgstr "Canlandırmalı WebP"

#~ msgid "Animated PNG"
#~ msgstr "Canlandırmalı PNG"

#~ msgid "Unknown image format: {}"
#~ msgstr "Bilinmeyen görüntü biçimi: {}"

#~ msgid "Could not open image"
#~ msgstr "Görüntü açılamadı"

#~ msgid "Could not read image"
#~ msgstr "Görüntü okunamadı"
